# Gulp plugin for minifying HTML literals

![npm](https://img.shields.io/npm/dt/gulp-minify-html-literals?label=Downloads)
![Maintenance](https://img.shields.io/maintenance/yes/2021?label=Maintained)
![npm](https://img.shields.io/npm/v/gulp-minify-html-literals?label=Version)
![npm](https://img.shields.io/npm/l/gulp-minify-html-literals?label=License)
![NodeJS](https://img.shields.io/node/v/gulp-minify-html-literals?style=flat&label=NodeJS)
![npm](https://img.shields.io/npm/collaborators/gulp-minify-html-literals?color=gold&label=Collaborators)

[<img src="https://media.giphy.com/media/ZDEZ9ECHhTVG5tWDVZ/giphy.gif" width="100px" />](https://www.linkedin.com/in/joskeuter/)

This plugin is a [Gulp] wrapper for [minify-html-literals].

Minify HTML in your ES2015 (or later) string literals, e.g. when using [polymer] or [lit-element] or custom [webcomponents] or other JavaScript files that uses ECMASCRIPT string literals with HTML.

## Getting started

```console
npm i gulp-minify-html-literals
```

## Example usage

Considering this source in `./my-widgets/BttgTestWidget.js`:

```javascript
class BttgTestWidget extends $.classes.Widget {

  constructor() {

    super();

    this.test = 'test';

  }

  template() {
    return html`
      <h1>Test 1</h1>
      <p>This is a ${this.test}.</p>
      <p>This HTML should be minified.</p>
      `;
  }

  render() {
    return this.template();
  }

  static get is() {
    return 'bttg-test-widget';
  }

}

$.classes.Widget.def(BttgTestWidget.is, BttgTestWidget);
```

you can minify both javascript and the template html when with this `gulpfile.js`:

```javascript
const {
  src,
  dest,
  parallel,
  series
} = require('gulp');

const gulpMinifyJsTemplate = require('gulp-minify-html-literals');

const distWidgets = () =>
  src([`./my-widgets/**/*.js`])
  .pipe(minifyJSTemplate()) //or with options minifyJSTemplate({...})
  .pipe(dest('./dist/js/widgets/'));

module.exports = {

  default: distWidgets

};

```

For the actual options, see [minify-html-literals].

### License

Gulp Minify HTML Literals is [MIT] licensed.

[![static](https://img.shields.io/badge/Company-BTTG-black?style=flat)](https://www.linkedin.com/in/joskeuter/)
[![Twitter Follow](https://img.shields.io/twitter/follow/bttg_)](https://twitter.com/bttg_)
[![static](https://img.shields.io/badge/Author-Jos%20Keuter-gold?style=flat)](https://www.linkedin.com/in/joskeuter/)
[![Twitter Follow](https://img.shields.io/twitter/follow/keuter)](https://twitter.com/Keuter)

[minify-html-literals]: https://www.npmjs.com/package/minify-html-literals
[through2]: https://www.npmjs.com/package/through2
[polymer]: https://polymer-library.polymer-project.org/3.0/docs/about_30
[lit-element]: https://lit-element.polymer-project.org/
[webcomponents]: https://www.webcomponents.org/
[gulp]: https://gulpjs.com/
[mit]: https://gitlab.com/bttg_/gulp-minify-html-literals/-/raw/master/LICENSE
