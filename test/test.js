//const assert = require('assert');
const expect = require('chai').expect;
//const should = require('chai').should;

const fs = require('fs');

describe('Test gulp-minify-html-literals', () => {

  before(() => require('./gulpfile').test());


  it('Created files should have compressed html string literals', () => {

    expect(fs.readFileSync(__dirname + '\\dist\\src-1.js', 'utf8')).to.contain('</h1><p>');
    expect(fs.readFileSync(__dirname + '\\dist\\src-2.js', 'utf8')).to.contain('</h1><p>');

  });

});